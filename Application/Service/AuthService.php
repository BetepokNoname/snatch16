<?php

namespace Application\Service;
use Core\Db\Adapter as DbAdapter;
use Core\Http\Request;
use Application\Service\UserService;
use Application\Model\User;

class AuthService
{

    /** @var Core\Db\Adapter */
    protected $db;
    protected $request;
    protected $user;
    protected $userService;
    
    public function setDbAdapter(DbAdapter $db)
    {
        $this->db = $db;
        
        return $this;
    }
    
    public function setRequest(Request $request)
    {
        $this->request = $request;

        return $this;
    }
    
    public function setUserService(UserService $service)
    {
        $this->userService = $service;

        return $this;
    }

    public function authenticate_api($credentials, $ip)
    {
        libxml_disable_entity_loader(true);

        // добавить еще безопаности

        $result = [];

        if ($this->getFailCount($ip) >= 5)
        {
            $local_result = array(
                'code' => -2,
                'error' => 'Too many attempts.'
            );
            array_push($result, $local_result);
            return json_encode($result); // превышено количество попыток            
        }

        try {
            $xml = simplexml_load_string($credentials); // надо проверить на XXE

            if ($xml === false) // если не распарсился
            {
                $local_result = array(
                    'code' => -3,
                    'error' => 'Failed while parsing XML.'
                );
                array_push($result, $local_result);
                return json_encode($result);
            }

            for ($i = 0; $i < count($xml->user); ++$i) // если распарсился, то проходим циклом по всем записям
            {
                $user = $this->userService->fetchByLogin($xml->user[$i]->login);
                if ((!$user) || ($user->getPassword() != md5($xml->user[$i]->password)))
                {
                    $this->saveLoginFail($xml->user->login, $ip);
                    $local_result = array(
                        'code' => -1,
                        'error' => 'Incorrect username or password.'
                    );
                    array_push($result, $local_result);
                    continue;
                }
                $local_result = array(
                    'code' => 1,
                    'error' => ''
                );
                array_push($result, $local_result);
                $_SESSION['user'] = array('id' => $user->getId());
                $this->user = $user;
            }
        } catch (Exception $e) {
            $result = [];
            $local_result = array(
                'code' => -4,
                'error' => 'Got exception while parsing XML.'
            );
            array_push($result, $local_result);
        }

        return json_encode($result);
    }
    
    public function authenticate($identity, $credential, $ip)
    {
        // убрать сохранение логинов при фейле

        if ($this->getFailCount($ip) >= 5)
            return -2; // превышено количество попыток

        $user = $this->userService->fetchByLogin($identity);
        if ((!$user) || ($user->getPassword() != md5($credential)))
        {
            $this->saveLoginFail($identity, $ip);
            return -1; // пароль или логин неправильный            
        }

        $_SESSION['user'] = array('id' => $user->getId());
        $this->user       = $user;

        return 1;
    }

    public function getFailCount($ip)
    {
        $delta_timestamp = 60; // seconds
        $sql = "SELECT count(*) FROM login_fails WHERE ip = ? AND timestamp > current_timestamp() - 60";
        $sth = $this->db->query($sql, $ip);
        return $sth->fetch()['count(*)'];
    }

    public function saveLoginFail($identity, $ip)
    {
        $sql = "INSERT INTO login_fails (login, ip, timestamp) VALUES (?, ?, current_timestamp())";
        $this->db->query($sql, $identity, $ip);
        return $this;
    }
    
    public function isAuthenticated()
    {
        if (empty($_SESSION['user']['id']))
            return false;
        
        if (!($this->user instanceof User))
            $this->user = $this->userService->fetchById($_SESSION['user']['id']);
        
        return true;
    }
    
    public function getUser()
    {
        return $this->user;
    }
    
    public function logout()
    {
        session_destroy();
    }
    
    public function changePassword($password)
    {
        $sql = "UPDATE users SET password = MD5(?), force_change_password = 0 WHERE id = ?";
        $this->db->query($sql, $password, $this->user->getId());
        
        return $this;
    }

}