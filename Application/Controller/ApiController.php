<?php

namespace Application\Controller;

use Core\Mvc\Controller\AbstractActionController;

include_once('messages.php');

class ApiController extends AbstractActionController
{
    protected $auth;
    protected $messages;
    protected $operator_ip;

    public function init()
    {
        libxml_disable_entity_loader(true);
        $this->auth = $this->serviceManager->get('auth');
        $this->messages = new \MessagesDB('localhost', 'root', '123', 'messages');
        $this->operator_ip = 'http://localhost';
    }

    public function authAction()
    {
        $layout = $this->application->getLayout();
        $layout->setLayout('Api');

        $response = $this->serviceManager->get('response');
        $response->setHeader('Content-Type', 'application/json');

        $request = $this->serviceManager->get('request');

        $result = json_encode(array(
            'code' => 0,
            'error' => 'Post data is empty'
        ));

        if ($request->isPost()) 
        {
            $post_data = file_get_contents('php://input');
            $ip = NULL;
            if ($_SERVER['HTTP_X_FORWARDED_FOR'] === NULL)
            {
                $ip = $_SERVER['REMOTE_ADDR'];
            }
            else
            {
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            }
            $result = $this->auth->authenticate_api($post_data, $ip);
        }

        $return['result'] = $result;
        return $return;
    }

    public function clearMessagesAction()
    {
        $layout = $this->application->getLayout();
        $layout->setLayout('Api');

        $response = $this->serviceManager->get('response');
        $response->setHeader('Content-Type', 'application/json');
        $response->setHeader('Access-Control-Allow-Origin', $this->operator_ip);
        $response->setHeader('Access-Control-Allow-Credentials', 'true');

        $request = $this->serviceManager->get('request');
        $token = $request->getParam('token');
        if ($token === '09e4c516d662526c2636da2e19201fa0')
        {
            $user = 'support';
            $this->messages->ClearMessages($user);

            $result = array(
                'code' => 1,
                'error' => '',
            );
            $return['result'] = json_encode($result);      
            return $return;
        }       

        $result = array(
            'code' => -1,
            'error' => 'Allow only for support',
        );
        $return['result'] = json_encode($result);      
        return $return;
    }

    public function operatorStuffAction()
    {
        $layout = $this->application->getLayout();
        $layout->setLayout('Api');

        $response = $this->serviceManager->get('response');
        $response->setHeader('Content-Type', 'application/json');
        $response->setHeader('Access-Control-Allow-Origin', $this->operator_ip);
        $response->setHeader('Access-Control-Allow-Credentials', 'true');

        $userService = $this->serviceManager->get('userService');

        $request = $this->serviceManager->get('request');
        $token = $request->getParam('token');
        if ($token === 'f7cc65e0c9b1b0c0822222e970663691')
        {
            $pans = $userService->getPANList();
            $result = array(
                'code' => 1,
                'error' => '',
                'array' => $pans
            );
            $return['result'] = json_encode($result);      
            return $return;
        }       

        $result = array(
            'code' => -1,
            'error' => 'Allow only for support',
            'array' => array()
        );
        $return['result'] = json_encode($result);      
        return $return;        
    }

    public function sendMessageAction()
    {
        $layout = $this->application->getLayout();
        $layout->setLayout('Api');

        $response = $this->serviceManager->get('response');
        $response->setHeader('Content-Type', 'application/json');

        if (!$this->auth->isAuthenticated()) // если пользователь не авторизован
        { 
            $result = array(
                'code' => -1,
                'error' => 'Not authorized',
            );
            $return['result'] = json_encode($result);
            return $return; 
        }

        $request = $this->serviceManager->get('request');

        $from_user = $this->auth->getUser()->getLogin();

        if ($request->isPost()) 
        {
            $to_user = $request->getPost('to');
            $message = $request->getPost('message');
            $hmac = $request->getPost('hmac');

            if ( (strlen($message) == 0) || (strlen($message) > 255) ) // проверяем длину message
            {
                $result = array(
                    'code' => -2,
                    'error' => 'Length of `message` should be more than 0 and less than 255',
                );
                $return['result'] = json_encode($result);
                return $return;  
            }

            if ( (strlen($to_user) == 0) || (strlen($to_user) > 64) ) // проверяем длину to_user
            {
                $result = array(
                    'code' => -3,
                    'error' => 'Length of `to` should be more than 0 and less than 64',
                );
                $return['result'] = json_encode($result);
                return $return;  
            }

            $SECRET = "SECRET_WILL_BE_HERE";
            //echo strtoupper(sha1("to={$to_user}&message={$message}" . $SECRET));
            if ( $hmac !== strtoupper(sha1("to={$to_user}&message={$message}" . $SECRET)) ) // проверяем hmac
            {
                $result = array(
                    'code' => -4,
                    'error' => 'hmac is incorrect',
                );
                $return['result'] = json_encode($result);
                return $return;                 
            }

            $this->messages->SendMessage($from_user, $to_user, $message); // заносим в БД
            $result = array(
                'code' => 1,
                'error' => '',
            );
            $return['result'] = json_encode($result);
            return $return; 
        }

        $result = array(
            'code' => 0,
            'error' => 'Post data is empty',
        );
        $return['result'] = json_encode($result);
        return $return;
    }

    public function getMessagesAction()
    {
        $layout = $this->application->getLayout();
        $layout->setLayout('Api');

        $response = $this->serviceManager->get('response');
        $response->setHeader('Content-Type', 'application/json');
        $response->setHeader('Access-Control-Allow-Origin', $this->operator_ip);
        $response->setHeader('Access-Control-Allow-Credentials', 'true');

        $request = $this->serviceManager->get('request');
        $token = $request->getParam('token');
        if ($token === 'e8e99d8235f894e761b3edd91b60c62d')
        {
            $user = 'support';
            $messagesArr = $this->messages->GetMessages($user);

            $result = array(
                'code' => 1,
                'error' => '',
                'messages' => $messagesArr
            );

            $return['result'] = json_encode($result);      

            return $return;
        }

        if (!$this->auth->isAuthenticated()) {
            $result = array(
                'code' => -1,
                'error' => 'Not authorized',
                'messages' => []
            );
            $return['result'] = json_encode($result);
            return $return; 
        }

        $user = $this->auth->getUser()->getLogin();
        $messagesArr = $this->messages->GetMessages($user);

        $result = array(
            'code' => 1,
            'error' => '',
            'messages' => $messagesArr
        );

        $return['result'] = json_encode($result);      

        return $return;
    }

    public function getInfoAction()
    {
        $layout = $this->application->getLayout();
        $layout->setLayout('Api');

        $response = $this->serviceManager->get('response');
        $response->setHeader('Content-Type', 'application/json');
        //$response->setHeader('Access-Control-Allow-Origin', 'http://localhost');
        //$response->setHeader('Access-Control-Allow-Credentials', 'true');

        if (!$this->auth->isAuthenticated()) {
            $result = array(
                'code' => -1,
                'error' => 'Not authorized',
                'info' => [],
                'accounts' => []
            );
            $return['result'] = json_encode($result);
            return $return; 
        }

        $user = $this->serviceManager->get('auth')->getUser();
        $userService = $this->serviceManager->get('userService');
        $accounts = $userService->fetchUserAccounts($user);
        $accountArray = [];

        foreach ($accounts as $account)
        {
            array_push($accountArray, array(
                'id' => $account->getId(),
                'account' => $account->getNumber(),
                'balance' => $account->getBalance(),
                'currency' => $account->getCurrency()
            ));
        }

        $infoArray = [];

        $infoArray['login'] = $user->getLogin();
        $infoArray['pan'] = $user->getPan();

        $result = array(
            'code' => 1,
            'error' => '',
            'info' => $infoArray,
            'accounts' => $accountArray
        );

        $return['result'] = json_encode($result);      

        return $return;
    }

    public function createTransactionAction()
    {
        $layout = $this->application->getLayout();
        $layout->setLayout('Api');

        $tService = $this->serviceManager->get('transactionService');

        $response = $this->serviceManager->get('response');
        $response->setHeader('Content-Type', 'application/json');

        if (!$this->auth->isAuthenticated()) {
            $result = array(
                'code' => -1,
                'error' => 'Not authorized',
                'transaction_id' => '',
            );
            $return['result'] = json_encode($result);
            return $return; 
        }

        $user = $this->serviceManager->get('auth')->getUser();
        $userService = $this->serviceManager->get('userService');
        $accounts = $userService->fetchUserAccounts($user);

        $request = $this->serviceManager->get('request');

        if ($request->isPost()) {
            try 
            {
                $from        = $request->getPost('from');
                $to          = $request->getPost('to');
                $sum         = $request->getPost('sum');
                $description = $request->getPost('description');

                $accountFrom = $userService->fetchAccountById($from);
                $accountTo   = $userService->fetchAccountByNumber($to);

                if (!$accountFrom)
                    throw new \Exception("Sender's account not found");
                if (!$accountTo)
                    throw new \Exception("Recipient's account not found");

                $transaction = $tService->createTransaction($user, $accountFrom, $accountTo, $sum, $description);
                
                if ( $user->getOtpMethod() === 'none' )
                {
                    $transaction->setConfirmed(true);
                    $tService->updateTransaction($transaction); 
                    $tService->commitTransaction($transaction->getId(), $user);
                }

                $result = array(
                    'code' => 1,
                    'error' => 'Is ok',
                    'transaction_id' => $transaction->getId(),
                    );
                $return['result'] = json_encode($result);
                return $return; 

            } 
            catch (\Exception $e) 
            {
                $result = array(
                    'code' => -2,
                    'error' => $e->getMessage(),
                    'transaction_id' => '',
                );
                $return['result'] = json_encode($result);
                return $return;  
            }
        }

        $result = array(
            'code' => 0,
            'error' => 'Post data is empty',
            'transaction_id' => '',
        );
        $return['result'] = json_encode($result);
        return $return;               
    }

    public function confirmMTanAction()
    {
        
        $layout = $this->application->getLayout();
        $layout->setLayout('Api');

        $tService = $this->serviceManager->get('transactionService');
        $userService = $this->serviceManager->get('userService');

        $response = $this->serviceManager->get('response');
        $response->setHeader('Content-Type', 'application/json');

        if (!$this->auth->isAuthenticated()) {
            $result = array(
                'code' => -1,
                'error' => 'Not authorized',
            );
            $return['result'] = json_encode($result);
            return $return; 
        }

        $user = $this->auth->getUser();

        $request = $this->serviceManager->get('request');
        if ($request->isPost()) 
        {
            try 
            {
                $id = $request->getPost('id');

                $transaction = $tService->fetchTransactionById($id);

                if (!$transaction)
                    throw new \Exception("Transcation not found");

                if ($transaction->getUserId() != $user->getId())
                    throw new \Exception("Forbidden");

                if ($transaction->getConfirmed())
                {
                    $result = array(
                        'code' => -3,
                        'error' => 'Transcation is confirmed already',
                        );
                    $return['result'] = json_encode($result);
                    return $return; 
                }              

                $accountFrom = $userService->fetchAccountById($transaction->getFrom());
                $accountTo   = $userService->fetchAccountById($transaction->getTo());

                if ($transaction->getOtpCode() == $request->getPost('otp')) 
                {
                    $transaction->setConfirmed(true);
                    $tService->updateTransaction($transaction); 
                    $tService->commitTransaction($id, $user);
                } 
                else 
                {
                    $result = array(
                        'code' => -4,
                        'error' => 'OTP is incorrect',
                        );
                    $return['result'] = json_encode($result);
                    return $return; 
                }

                $result = array(
                    'code' => 1,
                    'error' => 'Is ok',
                    );
                $return['result'] = json_encode($result);
                return $return; 

            } 
            catch (\Exception $e) 
            {
                $result = array(
                    'code' => -2,
                    'error' => $e->getMessage(),
                );
                $return['result'] = json_encode($result);
                return $return;  
            }
        }

        $result = array(
            'code' => 0,
            'error' => 'Post data is empty',
        );
        $return['result'] = json_encode($result);
        return $return;       
    }
}