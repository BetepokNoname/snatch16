<?php

namespace Application\Controller;

use Core\Mvc\Controller\AbstractActionController;

class IndexController extends AbstractActionController
{
    // Если пользователь авторизован, то шлем его в /account
    // Иначе в /auth/login для авторизации.
    public function indexAction()
    {
        $auth = $this->serviceManager->get('auth');
        if (!$auth->isAuthenticated()) {
            $this->redirect('/auth/login');
        }

        $this->redirect('/account');
    }

    // Переключение между мобильной версией сайта и обычной
    // Бекенд определяет версию по куке mobileInterface.
    // Для мобильной версии она True. Для обычной её вообще нет.
    /*public function switchInterfaceAction() 
    {
        $request = $this->serviceManager->get('request');

        if ($request->getCookie('mobileInterface')) {
            setcookie('mobileInterface', '', null, '/');
        } else {
            setcookie('mobileInterface', 'true', null, '/');
        }

        $this->redirect('/');
    }*/

}