<?php

class MessagesDB
{
	private $db;

	function __construct($host, $user, $pass, $db)
	{
		//mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT); // for debug
		$this->db = new mysqli($host, $user, $pass, $db);
		echo mysqli_connect_error();
		if ( !$this->db ) 
		{
    		printf("Connect to DB failed: %s\n", mysqli_connect_error());
    		exit();
		}
	}

	private function isConnected()
	{
		if ($this->db->connect_error)
		{
			printf("There are any errors with DB connection!");
   			return false;
		}
		else 
   			return true;
	}

	public function SendMessage($from, $to, $data)
	{
		$from_user = $from;
		if ( $this->isConnected() )
		{
			$sql = $this->db->prepare("INSERT INTO `messages` (`from`,`to`,`data`,`timestamp`) VALUES(?, ?, ?, now())");
			$sql->bind_param('sss', $from_user, $to, $data);
			$sql->execute();
			$sql->close(); 			
		}
	}

    public function ClearMessages($user)
    {
        if ( $this->isConnected() )
        {
            $query = "DELETE FROM messages WHERE `to` = '" . $this->db->real_escape_string($user) . "' OR `from` = '" . $this->db->real_escape_string($user) . "';";
            $this->db->query($query);          
        }        
    }

	public function GetMessages($user)
	{
		if ( $this->isConnected() )
		{
			$query = "SELECT `id`,`from`,`to`,`data`,`timestamp` FROM messages WHERE `from` = '" . $this->db->real_escape_string($user) . "' OR `to` = '" . $this->db->real_escape_string($user) . "'";
			$result = $this->db->query($query);
			$array = array();
    		while ($row = $result->fetch_row()) 
    		{
    			$record = array(
    				//"id" => $row[0],
    				"from" => $row[1],
    				"to" => $row[2],
    				"data" => $row[3],
    				"timestamp" => $row[4],
    			);
    			array_push($array, $record);
    		}
    		$result->close();
    		return $array;
		}
	}
}

?>