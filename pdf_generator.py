import datetime
import mysql.connector
from fpdf import FPDF

cnx = mysql.connector.connect(user='root', password='123', database='bank')
cursor = cnx.cursor()

query = ("SELECT id,login,password,email,first_name,last_name FROM users")

cursor.execute(query)

for i in cursor:
	id = i[0]
	login = i[1]
	password = i[2]
	email = i[3]
	first_name = i[4]
	last_name = i[5]
	pdf = FPDF()
	pdf.add_page()

	pdf.set_font('Arial', '', 18)
	pdf.cell(0, 10, 'Requisites', 0, 1, 'C')
	pdf.set_font('Arial', '', 14)

	pdf.cell(0, 10, 'Information', 0, 1, 'C')
	pdf.cell(20, 7, 'Login: %s' % login, 0, 1)
	pdf.cell(20, 7, 'Email: %s' % email, 0, 1)
	#pdf.cell(20, 7, 'Phone: %s' % phone, 0, 1)
	pdf.cell(20, 7, 'First name: %s' % first_name, 0, 1)
	pdf.cell(20, 7, 'Last name: %s' % last_name, 0, 1)

	#pdf.cell(0, 10, 'Accounts', 0, 1, 'C')
	#for account in accounts:
	#	pdf.cell(20, 7, '%s (%s)' % (account['id'], account['type']), 0, 1)

	pdf.output('public/docs/%s.pdf' % id, 'F')

cursor.close()
cnx.close()