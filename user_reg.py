import datetime
import mysql.connector
from fpdf import FPDF
import random
import string
import json
import hashlib

def password_generator(size=16, chars=string.ascii_uppercase + string.digits + string.ascii_lowercase):
    return ''.join(random.choice(chars) for _ in range(size))

def login_generator(size=9, chars=string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

def pan_generator(size=16, chars=string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

def phone_generator(size=7, chars=string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

def account_generator(size=20, chars=string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

cnx = mysql.connector.connect(user='root', password='123', database='bank')
cursor = cnx.cursor()

#INSERT INTO `users` (`login`, `password`, `otp_method`, `email`, `first_name`, `last_name`, `phone`, `pan`) VALUES ("qweqwe", "d8578edf8458ce06fbc5bb76a58c5ca4", "none", "emtpy", "empty", "empty", "empty", "1111222233334444");
#SELECT id FROM users WHERE login = "qweqwe";
#INSERT INTO `accounts` (`user_id`, `number`, `balance`, `currency`) VALUES ('32', '90107430600227300333', 0, 'rub');

with open('users.json') as data_file:  
    data = json.load(data_file)

for i in data:
    login = login_generator();
    otp_method = i['otp_method']
    #if otp_method == 'rtan':
    #    password = password_generator()
    #else:
    #    password = i['password']
    password = i['password']
    pan = pan_generator()
    account = account_generator()
    balance = i['balance']
    first_name = i['first_name']
    last_name = i['last_name']
    email = i['email']
    phone = i['phone']

    if phone == '88005553535':
        phone = '8800' + phone_generator()

    #print 'Login: %s' % login
    #print 'Password: %s' % password
    #print 'PAN: %s' % pan
    #print 'Account: %s' % account
    #print 'Money: %s' % balance
    #print 'Info: %s %s %s %s' % (first_name, last_name, email, phone)
    #print '\n-----\n'

    query = ('INSERT INTO `users` (`login`, `password`, `otp_method`, `email`, `first_name`, `last_name`, `phone`, `pan`) VALUES ("%s", "%s", "%s", "%s", "%s", "%s", "%s", "%s");' % (login, hashlib.md5(password).hexdigest(), otp_method, first_name, last_name, email, phone, pan))
    cursor.execute(query)
    cnx.commit()

    query = ('SELECT id FROM users WHERE login = "%s"' % login)
    cursor.execute(query)
    for i in cursor:
        user_id = i[0]

    query = ('INSERT INTO `accounts` (`user_id`, `number`, `balance`, `currency`) VALUES ("%d", "%s", "%d", "rub");' % (user_id, account, balance))
    cursor.execute(query)
    cnx.commit()

    if otp_method == 'rtan' or otp_method == 'tan':
        query = ('INSERT INTO `rel_users_services` (`user_id`, `service_id`) VALUES ("%d", 1);' % user_id)
        cursor.execute(query)
        cnx.commit() 




N = 150 # player accounts

print '---------- Empty accounts: ----------\n'
for i in range(0, N):
    login = login_generator();
    password = password_generator()
    pan = pan_generator()
    account = account_generator()

    print '%s:%s' % (login,password)
    #print 'Login: %s' % login
    #print 'Password: %s' % password
    #print 'Hash: %s' % hashlib.md5(password).hexdigest()
    #print 'PAN: %s' % pan
    #print 'Account: %s' % account
    #print '\n-----\n' 

    query = ('INSERT INTO `users` (`login`, `password`, `otp_method`, `email`, `first_name`, `last_name`, `phone`, `pan`) VALUES ("%s", "%s", "none", "empty", "empty", "empty", "empty", "%s");' % (login, hashlib.md5(password).hexdigest(), pan))
    cursor.execute(query)
    cnx.commit()

    query = ('SELECT id FROM users WHERE login = "%s"' % login)
    cursor.execute(query)
    for i in cursor:
        user_id = i[0]

    query = ('INSERT INTO `accounts` (`user_id`, `number`, `balance`, `currency`) VALUES ("%d", "%s", 0, "rub");' % (user_id, account))
    cursor.execute(query)
    cnx.commit()

    #query = ('INSERT INTO `rel_users_services` (`user_id`, `service_id`) VALUES ("%d", 1);' % user_id)
    #cursor.execute(query)
    #cnx.commit()


cursor.close()
cnx.close()
#query = ("SELECT id,login,password,email,first_name,last_name FROM users")

#cursos.execute(query)


