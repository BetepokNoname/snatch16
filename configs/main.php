<?php

return array(
    'application' => array(
        'displayExceptions' => true,
        'viewHelpers'       => array(
            'accountBalance' => 'Application\View\Helper\AccountBalance',
            'currencySymbol' => 'Application\View\Helper\CurrencySymbol',
            'showIncome'     => 'Application\View\Helper\showIncome',
        ),
    ),
    'database'    => array(
        'user'     => 'bank',
        'password' => '123',
        'host'     => 'localhost',
        'port'     => '3306',
        'dbname'   => 'bank',
    ),
    'rates'       => array(
        'rub>usd' => 1 / 76,
        'usd>rub' => 74,
        'rub>rub' => 1,
        'usd>usd' => 1,
    ),
);
